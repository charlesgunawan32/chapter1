//stdin-stdout
const readline = require('readline').createInterface({
    input: process.stdin,
    output: process.stdout,
});

//fungsi input keyboard
function input(question){
    return new Promise(resolve => {
        readline.question(question, data=>{
            return resolve(data);
        });
    });
}

//fungsi penjumlahan
async function penjumlahan(){
    let hasil = 0;
    let berhenti = false;
    do{
        nums = await input("Input angka yang ingin dijumlahkan = ");
        hasil += +nums;
        console.log("Ada yang ingin ditambahkan lagi?");
        console.log("1. Iya");
        console.log("2. Tidak");
        tambahAngka = await input("Pilihan = ");
        if(+tambahAngka == 2){
            berhenti=true;
        };
    } while(!berhenti);
    console.log("Hasilnya adalah = " + hasil);
}

//fungsi pengurangan
async function pengurangan(){
    let hasil = 0;
    let berhenti = false;
    nums = await input("Input angka yang ingin dikurangi = ");
    hasil = +nums;
    do{
        pengurang = await input("Input angka sebagai pengurang = ");
        hasil -= +pengurang;
        console.log("Ingin mengurangi lagi?");
        console.log("1. Iya");
        console.log("2. Tidak");
        tambahAngka = await input("Pilihan = ");
        if(+tambahAngka == 2){
            berhenti=true;
        };
    } while(!berhenti);
    console.log("Hasilnya adalah = " + hasil);
}

//fungsi pengalian
async function pengalian(){
    let hasil = 0;
    let berhenti = false;
    nums = await input("Input angka yang ingin dikalikan = ");
    hasil = +nums;
    do{
        pengali = await input("Input angka sebagai pengali = ");
        hasil *= +pengali;
        console.log("Ingin dikalikan lagi?");
        console.log("1. Iya");
        console.log("2. Tidak");
        tambahAngka = await input("Pilihan = ");
        if(+tambahAngka == 2){
            berhenti=true;
        };
    } while(!berhenti);
    console.log("Hasilnya adalah = " + hasil);
}

//fungsi pembagian
async function pembagian(){
    let hasil = 0;
    let berhenti = false;
    nums = await input("Input angka yang ingin dibagi = ");
    hasil = +nums;
    do{
        pembagi = await input("Input angka sebagai pembagi = ");
        hasil /= +pembagi;
        console.log("Ingin dibagi lagi?");
        console.log("1. Iya");
        console.log("2. Tidak");
        tambahAngka = await input("Pilihan = ");
        if(+tambahAngka == 2){
            berhenti=true;
        };
    } while(!berhenti);
    console.log("Hasilnya adalah = " + hasil);
}

//fungsi akar kuadrat
async function akarKuadrat(){
    angka = await input("Input angka yang ingin diakarkan = ");
    hasil = Math.sqrt(+angka);
    console.log("Hasilnya adalah = " + hasil);
}

//fungsi luas persegi
async function luasPersegi(){
    sisi = await input("Input sisi persegi = ");
    sisi *= sisi;
    console.log("Hasilnya adalah = " + sisi);
}

//fungsi volume kubus
async function volumeKubus(){
    rusuk = await input("Input rusuk kubus = ");
    hasil = rusuk**3;
    console.log("Hasilnya adalah = " + hasil);
}

//fungsi volume tabung
async function volumeTabung(){
    const PI = 3.14;
    let benar = false;
    do{
        console.log("Apa yang diketahui?");
        console.log("1. Jari-jari");
        console.log("2. Diameter");
        pilihan = await input("Pilihan = ");
        if(+pilihan == 1){
            r = await input("Input jari-jari lingkaran pada tabung = ");
            benar = true;
        } else if(+pilihan==2){
            d = await input("Input diameter lingkaran pada tabung = ");
            r=d/2;
            benar = true;
        } else {
            console.log("Pilihan tidak ada");
            console.log("");
        };
    } while(!benar);
    t = await input("Input tinggi pada tabung = ");
    hasil = PI*r*r*t;
    console.log("Hasilnya adalah = " + hasil);
}

//fungsi main untuk program
async function main(){
    let stop = false;
    do{
        console.log("");
        console.log("MENU");
        console.log("1. Penjumlahan");
        console.log("2. Pengurangan");
        console.log("3. Pengalian");
        console.log("4. Pembagian");
        console.log("5. Akar Kuadrat");
        console.log("6. Luas Persegi");
        console.log("7. Volume Kubus");
        console.log("8. Volume Tabung");
        console.log("0. Stop");
        console.log("");
        menu = await input("Pilih menu berapa = ");
            switch(+menu){
                case 1:
                    await penjumlahan();
                    break;
                case 2:
                    await pengurangan();
                    break;
                case 3:
                    await pengalian();
                    break;
                case 4:
                    await pembagian();
                    break;
                case 5:
                    await akarKuadrat();
                    break;
                case 6:
                    await luasPersegi();
                    break;
                case 7:
                    await volumeKubus();
                    break;
                case 8:
                    await volumeTabung();
                    break;
                case 0:
                    stop=true;
                    readline.close();
                    break;
                default:
                    console.log("Menu tidak tersedia");
                    break;
            }
        }while(!stop);
}

//pemnaggilan fungsi main
main();